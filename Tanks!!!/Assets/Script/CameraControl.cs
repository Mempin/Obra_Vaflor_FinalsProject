﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Extensions;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class CameraControl : MonoBehaviour
    {
        public float DampTime = 0.2f;
        public float ScreenEdgeBuffer = 4f;
        public float MinSize = 6.5f;
        
        public Transform[] Targets;

        private UnityEngine.Camera camera
        {
            get {return GetComponentInChildren<UnityEngine.Camera>();}
        }

        private float zoomSpeed;
        private Vector3 moveVelocity;
        private Vector3 desiredPosition;

        private void FixedUpdate()
        {
            Move();
            Zoom();
        }

        private void Move()
        {
            FindAveragePosition();
            transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, DampTime);
        }

        private void FindAveragePosition()
        {
            var averagePos = new Vector3();
            var numTargets = 0;
            foreach (var target in Targets.Where(t => t.IsGameObjectActive()))
            {
                averagePos += target.position;
                numTargets++;
            }

            if (numTargets > 0)
            {
                averagePos /= numTargets;
            }
            averagePos.y = transform.position.y;
            desiredPosition = averagePos;
        }

        private void Zoom()
        {
            var requiredSize = FindRequiredSize();
            camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, requiredSize, ref zoomSpeed, DampTime);
        }

        private float FindRequiredSize()
        {
            var desiredLocalPos = transform.InverseTransformPoint(desiredPosition);
            var size = CalculateSizeForEachTarget(desiredLocalPos);
            return CalculateScreenBuffer(size);
        }

        private float CalculateSizeForEachTarget(Vector3 desiredLocalPos)
        {
            var size = 0f;
            foreach (var desiredPosToTarget in GetDesiredTargetPositions(desiredLocalPos))
            {
                size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
                size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / camera.aspect);
            }
            return size;
        }

        private IEnumerable<Vector3> GetDesiredTargetPositions(Vector3 desiredLocalPos)
        {
            return Targets.Where(t => t.gameObject.activeSelf)
                    .Select(target => transform.InverseTransformPoint(target.position))
                    .Select(targetLocalPos => targetLocalPos - desiredLocalPos);
        }

        private float CalculateScreenBuffer(float size)
        {
            return Mathf.Max(size + ScreenEdgeBuffer, MinSize);
        }

        public void SetStartPositionAndSize()
        {
            FindAveragePosition();
            transform.position = desiredPosition;
            camera.orthographicSize = FindRequiredSize();
        }
    }
}