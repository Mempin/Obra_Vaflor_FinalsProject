﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

	public GameObject player;
	public Rigidbody Shell;                   // Prefab of the shell.
	public Transform FireTransform;           // A child of the tank where the shells are spawned.
	public AudioSource ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
	public AudioClip ChargingClip;            // Audio that plays when each shot is charging up.
	public AudioClip FireClip;                // Audio that plays when each shot is fired.
	public float MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
	public float MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
	public float MaxChargeTime = 0.75f;       // How long the shell should charge for before it is fired at max force.
	
	public float ForceError = 2f;             // The error of launch force used to simulate keyboard input.
    public Transform TargetPlayer;           // Attack target, assigned by GameManager class.
	
	private float CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
	private float ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
	private float CurrentChargeTime;          // Record indicates how long the shell has been charged.
	private bool Fired;                       // Whether or not the shell has been launched with this button press.
	private bool ShellIsCharging;             // Whether or not the shell is being charged.
	
	private void OnEnable(){
		// When the enemy is turned on, reset the launch force
		CurrentLaunchForce = MinLaunchForce;
	}
	
	// Use this for initialization
	private void Start () {
		// The rate that the launch force charges up is the range of possible forces by the max charge time.
		ChargeSpeed = (MaxLaunchForce - MinLaunchForce) / MaxChargeTime;
		player = GameObject.FindGameObjectWithTag ("Player");
		
	}
	
	
	private void Update () {
		// Calculate the distance from this enemy to the target.
		float targetDistance = (TargetPlayer.position-transform.position).magnitude;
		
		// If the max force has been exceeded and the shell hasn't yet been launched...
		if (CurrentLaunchForce >= MaxLaunchForce && !Fired)
		{
			// ... use the max force and launch the shell.
			CurrentLaunchForce = MaxLaunchForce;
			Fire ();
		}
		// Otherwise, if the shell has just started being charged...
		else if (ShellChargeStart(targetDistance))
		{
			// ... reset the fired flag and reset the launch force.
			Fired = false;
			CurrentLaunchForce = MinLaunchForce;
			
			// Change the clip to the charging clip and start it playing.
			ShootingAudio.clip = ChargingClip;
			ShootingAudio.Play ();
		}
		// Otherwise, if the shell is charging and it hasn't been launched yet...
		else if (ShellIsCharging && !Fired)
		{
			// Increment the launch force.
			CurrentLaunchForce += ChargeSpeed * Time.deltaTime;
		}
		// Otherwise, if the shell is charged and it hasn't been launched yet...
		else if (ShellChargeComplete (targetDistance) && !Fired)
		{
			// ... launch the shell.
			Fire ();
		}
	}
	
	
	private bool ShellChargeStart(float distance){
		// If the target is close enough...
		if (distance < MaxLaunchForce)
		{
			// ... and the shell is not being charged.
			if (!ShellIsCharging)
			{
				// ... confirm to start charging the shell.
				ShellIsCharging = true;
				return true;
			}
			// Otherwise, the shell is already charging, ...
			else
				// ... so keep charging.
				return false;
		}
		// Otherwise, target is too far away...
		else
		{
			// ... then make sure the shell is not charging.
			ShellIsCharging = false;
			return false;
		}
	}
	
	
	private bool ShellChargeComplete(float distance){
		// Make sure the shell is charging.
		if (ShellIsCharging)
		{
			// Reset the charging flag.
			ShellIsCharging = false;
			
			// If the shell is over-charged, it is charged completely.
			return CurrentLaunchForce > distance;
		}
		// Otherwise, no way we can talk about "complete".
		else
			return false;
	}
	
	private void Fire(){
		// Set the fired flag so the Fire function is only called once.
		Fired = true;
		
		// Create an instance of the shell and store a reference to its rigidbody.
		Rigidbody shellInstance =
			Instantiate(Shell, FireTransform.position, FireTransform.rotation) as Rigidbody;
		
		// Set the shell's velocity to the launch force in the fire position's forward direction.
		// Also add some random interference.
		shellInstance.velocity =
			(CurrentLaunchForce + Random.Range(-ForceError, ForceError)) * FireTransform.forward;
		
		// Change the clip to the firing clip and play it.
		ShootingAudio.clip = FireClip;
		ShootingAudio.Play();
		
		// Reset the launch force.
		CurrentLaunchForce = MinLaunchForce;
	}
}