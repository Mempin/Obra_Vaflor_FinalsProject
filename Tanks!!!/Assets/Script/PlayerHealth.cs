﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Objects.Tank
{
    public class PlayerHealth : MonoBehaviour
    {
        public float StartingHealth = 100f;
        public Slider Slider;
        public Image FillImage;
        public Color FullHealthColor = Color.green;
        public Color ZeroHealthColor = Color.red;
        public GameObject ExplosionPrefab;

        private AudioSource explosionAudio;
        private ParticleSystem explosionParticles;
        private float currentHealth;
        private bool isDead;


        private void Awake()
        {
            explosionParticles = Instantiate(ExplosionPrefab).GetComponent<ParticleSystem>();
            explosionAudio = explosionParticles.GetComponent<AudioSource>();

            explosionParticles.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            currentHealth = StartingHealth;
            isDead = false;

            SetHealthUI();
        }

        public void TakeDamage(float amount)
        {
            
            currentHealth -= amount;
            SetHealthUI();
            if (currentHealth <= 0f && !isDead)
            {
                OnDeath();
            }
        }

        private void SetHealthUI()
        {

            Slider.value = currentHealth;
            FillImage.color = Color.Lerp(ZeroHealthColor, FullHealthColor, currentHealth/StartingHealth);
        }

        private void OnDeath()
        {

            isDead = true;
            PlayExplosionParticles();
            PlayExplosionAudio();
            gameObject.SetActive(false);
        }

        private void PlayExplosionParticles()
        {
            explosionParticles.transform.position = transform.position;
            explosionParticles.gameObject.SetActive(true);
            explosionParticles.Play();
        }

        private void PlayExplosionAudio()
        {
            explosionAudio.Play();
        }
    }
}