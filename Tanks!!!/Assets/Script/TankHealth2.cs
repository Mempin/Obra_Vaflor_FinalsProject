﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Objects.Tank
{
    public class TankHealth2 : MonoBehaviour
    {
        public float StartingHealth = 100f;
        public Slider Slider;
        public Image FillImage;
        public Color FullHealthColor = Color.green;
        public Color ZeroHealthColor = Color.red;
        public GameObject ExplosionPrefab;
		public int enemyCount = 1;
		public Text enemyText;

        private AudioSource explosionAudio;
        private ParticleSystem explosionParticles;
        private float currentHealth;
        private bool isDead;
		private int killCount;




      //  private void Awake()
       // {
      //      explosionParticles = Instantiate(ExplosionPrefab).GetComponent<ParticleSystem>();
      //      explosionAudio = explosionParticles.GetComponent<AudioSource>();

       //     explosionParticles.gameObject.SetActive(false);
       // }

        private void OnEnable()
        {

			killCount = enemyCount;
            currentHealth = StartingHealth;
            isDead = false;
			CheckEnemy ();
            SetHealthUI();
        }

        public void TakeDamage(float amount)
        {
            
            currentHealth -= amount;
            SetHealthUI();
            if (currentHealth <= 0f && !isDead)
            {
                OnDeath();
            }
        }

        private void SetHealthUI()
        {

            Slider.value = currentHealth;
            FillImage.color = Color.Lerp(ZeroHealthColor, FullHealthColor, currentHealth/StartingHealth);
        }

        private void OnDeath()
        {
			Destroy (gameObject);
	

		
			if (gameObject.CompareTag ("Player")) {
			
				Application.LoadLevel ("GameOver2");
			} else if (gameObject.CompareTag ("Enemy")) {
		
				//PlayExplosionParticles ();
				//PlayExplosionAudio ();
				Destroy (gameObject);
				killCount--;
				CheckEnemy ();


			} 






        }

		private void onPlayerDeath()
		{

		}

        private void PlayExplosionParticles()
        {
            explosionParticles.transform.position = transform.position;
            explosionParticles.gameObject.SetActive(true);
            explosionParticles.Play();
        }

        private void PlayExplosionAudio()
        {
            explosionAudio.Play();
        }

		public void CheckEnemy()
		{
			enemyText.text = "Boss: " + killCount ;

			if (killCount == 0) 
			{
				Application.LoadLevel ("Clear2");
			}
		}
    }

}