﻿using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts.Objects.Tank
{
    public class TankMovement : MonoBehaviour
    {
        public int PlayerNumber = 1;         
        public float Speed = 12f;            
        public float TurnSpeed = 180f;
        public AudioSource MovementAudio;
        public AudioClip EngineIdling;
        public AudioClip EngineDriving;
        public float PitchRange = 0.2f;

        private string movementAxisName;
        private string turnAxisName;
        private Rigidbody rigidbody;
        private float movementInputValue;
        private float turnInputValue;
        private float originalPitch;
        private TankStates State;

        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
            State = TankStates.Idle;
        }

        private void OnEnable ()
        {
            rigidbody.isKinematic = false;
            movementInputValue = 0f;
            turnInputValue = 0f;
        }

        private void OnDisable ()
        {
            rigidbody.isKinematic = true;
        }

        private void Start()
        {
            movementAxisName = "Vertical" + PlayerNumber;
            turnAxisName = "Horizontal" + PlayerNumber;

            originalPitch = MovementAudio.pitch;
        }

        private void Update()
        {
            HandleInput();
            SetState();
            EngineAudio();
        }

        private void HandleInput()
        {
            movementInputValue = Input.GetAxis(movementAxisName);
            turnInputValue = Input.GetAxis(turnAxisName);
        }

        private void EngineAudio()
        {
            
            if (Mathf.Abs(movementInputValue) < 0.1f && Mathf.Abs(turnInputValue) < 0.1f)
            {
                if (MovementAudio.clip == EngineDriving)
                {
                    MovementAudio.clip = EngineIdling;
                    MovementAudio.pitch = Random.Range(originalPitch - PitchRange, originalPitch + PitchRange);
                    MovementAudio.Play();
                }
            }
            else if (MovementAudio.clip == EngineIdling)
            {
                MovementAudio.clip = EngineDriving;
                MovementAudio.pitch = Random.Range(originalPitch - PitchRange, originalPitch + PitchRange);
                MovementAudio.Play();
            }
        }

        private void FixedUpdate()
        {
            
            Move();
            Turn();
        }

        private void Move()
        {
            
            var movement = transform.forward * movementInputValue * Speed * Time.deltaTime;
            rigidbody.MovePosition(rigidbody.position + movement);
            
        }

        private void Turn()
        {
           
            var turnAmount = turnInputValue * TurnSpeed * Time.deltaTime;
            var turnRotation = Quaternion.Euler(0f, turnAmount, 0);
            rigidbody.MoveRotation(rigidbody.rotation * turnRotation);
        }

        private void SetState()
        {
            if (movementInputValue > 0f)
            {
                State = TankStates.Moving;
            }
            else
            {
                State = TankStates.Idle;
            }
        }
    }
}