﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Objects.Tank
{
    public class TankShooting : MonoBehaviour
    {
        public int PlayerNumber = 1;
        public Rigidbody Shell;
        public Transform FireTransform;
        public AudioSource ShootingAudio;
        public AudioClip ChargingClip;
        public AudioClip FireClip;
        public float MinLaunchForce = 15f;
        public float MaxLaunchForce = 30f;
        public float MaxChargeTime = 0.75f;

        private string fireButton;
        private float currentLaunchForce;
        private float chargeSpeed;
        private bool hasFired;

        private void OnEnable()
        {
            currentLaunchForce = MinLaunchForce;
        }

        private void Start()
        {
            fireButton = "Fire" + PlayerNumber;
            chargeSpeed = (MaxLaunchForce - MinLaunchForce) / MaxChargeTime;
        }

        private void Update()
        {
          
            if (currentLaunchForce >= MaxLaunchForce && !hasFired)
            {

                currentLaunchForce = MaxLaunchForce;
                Fire();
            }
            else if (Input.GetButtonDown(fireButton))
            {

                hasFired = false;
                currentLaunchForce = MinLaunchForce;
                PlayChargingAudio();
            }
            else if (Input.GetButton(fireButton) && !hasFired)
            {

                currentLaunchForce += chargeSpeed * Time.deltaTime;
            }
            else if (Input.GetButtonUp(fireButton) && !hasFired)
            {

                Fire();
            }
        }

        private void PlayChargingAudio()
        {
            ShootingAudio.clip = ChargingClip;
            ShootingAudio.Play();
        }

        private void PlayFireAudio()
        {
            ShootingAudio.clip = FireClip;
            ShootingAudio.Play();
        }

        private void Fire()
        {

            hasFired = true;
            var shellInstance = Instantiate(Shell, FireTransform.position, FireTransform.rotation) as Rigidbody;
            shellInstance.velocity = currentLaunchForce * FireTransform.forward;
            PlayFireAudio();
            currentLaunchForce = MinLaunchForce;
        }
    }
}